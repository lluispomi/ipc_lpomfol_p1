import 'package:flutter/material.dart';
import 'package:lpomfol_practicas/login_view.dart';

class paginaProductos extends StatelessWidget {
  const paginaProductos({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PRODUCTOS"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Vuelta a la pagina principal'),
        ),
      ),
    );
  }
}
