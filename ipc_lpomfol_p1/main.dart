import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lpomfol_practicas/login_view.dart';
import 'package:lpomfol_practicas/products_list_view.dart';

void main() => runApp(MiApp());

class MiApp extends StatelessWidget {
  const MiApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:
          false, //quita una pestaña de arriba a la derecha
      title: "P1 IPC", //Nombre de la app
      home: inicioSesion(),
    );
  }
}
