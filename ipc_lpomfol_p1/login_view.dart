import 'package:flutter/material.dart';
import 'package:lpomfol_practicas/products_list_view.dart';

class inicioSesion extends StatelessWidget {
  inicioSesion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          " Login",
          style: TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 36.0),
        ),
      ),
      body: cuerpo(context),
    );
  }
}

class inicio extends StatefulWidget {
  const inicio({super.key});

  @override
  State<inicio> createState() => _inicioState();
}

class _inicioState extends State<inicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          " Login",
          style: TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 36.0),
        ),
      ),
      body: Center(
        child: cuerpo(context),
      ),
    );
  }
}

Widget cuerpo(BuildContext context) {
  return Container(
      child: Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        nombre(),
        campoUsuaruio(),
        campoContrasena(),
        SizedBox(
          height: 30,
        ),
        botonInicio(context),
      ],
    ),
  ));
}

Widget nombre() {
  return const Text(
    "Inicia sesión",
    style: TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 36.0),
  );
}

Widget campoUsuaruio() {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
    child: TextField(
      decoration: InputDecoration(
        hintText: "Usuario",
      ),
    ),
  );
}

Widget campoContrasena() {
  return Container(
    padding: EdgeInsets.symmetric(
        horizontal: 30,
        vertical:
            20), //Para Dejar un hueco en el contorno del cuadrado de inicio de sesion.
    child: TextField(
      obscureText: true, // Para que se esconda la contraseña al escribir
      decoration: InputDecoration(
        hintText: "Contrasena",
      ),
    ),
  );
}

Widget botonInicio(BuildContext context) {
  return TextButton(
    child: Text("Entrar"),
    style: ButtonStyle(
      foregroundColor:
          MaterialStateProperty.all<Color>(Color.fromARGB(255, 218, 11, 218)),
    ),
    onPressed: () {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => paginaProductos()));
    },
  );
}
